import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
  addTodo,
  delTodo,
  updateTodo,
  allChange,
  addTodoAsync
} from '../../redux/actions/todolist'
import ClearDone from '../clearDone'

function TodoList(props) {
  // console.log(props)
  const { addTodo, delTodo, updateTodo, allChange, addTodoAsync, todolists } =
    props
  // useState 不能用在循环
  const [mouseI, setMouseI] = useState(-1)
  const [tabIndex, setTabIndex] = useState(0)
  const [footerList, setFooterList] = useState(['all', 'active', 'Completed'])
  useEffect(() => {
    localStorage.setItem('react-todolist', JSON.stringify(todolists))
  }, [todolists])
  //   getSnapshotBeforeUpdate(prevProps, prevState) {
  //     return this.props.todolists
  //   }
  //   //更新的生命周期
  //   componentDidUpdate(prevProps, prevState, todos) {
  //     localStorage.setItem('react-todolist', JSON.stringify(todos))
  //   }
  //添加todo
  const handleAddTodo = e => {
    const { keyCode, target } = e
    if (keyCode !== 13) {
      return
    } //13是enter的标识符
    const todoObj = { id: Date.now(), name: target.value, done: false }
    addTodoAsync(todoObj, 200) //异步rudux  在actions添加
    target.value = ''
  }
  //删除todo
  const handleDeleteTodo = id => {
    if (window.confirm('确认删除么？')) {
      delTodo(id)
    }
  }
  //更新todo
  const handleUpdateTodo = (id, done) => {
    updateTodo({ id, done })
  }
  const handleAllChange = done => {
    allChange(done)
  }

  const filter_TodoLists = () => {
    switch (tabIndex) {
      case 1:
        return todolists.filter(item => !item.done)
      case 2:
        return todolists.filter(item => item.done)
      default:
        return todolists
    }
  }
  const doneLenth = todolists.reduce(
    (pre, current) => pre + (current.done ? 1 : 0),
    0
  ) //已完成的
  return (
    <div className='todo-container'>
      <div className='todo-wrap'>
        <div className='todo-header'>
          <input
            type='text'
            onKeyUp={e => handleAddTodo(e)}
            placeholder='请输入你的任务名称，按回车键确认'
          />
        </div>
        <ul className='todo-main'>
          {/* 过滤数组 */}
          {filter_TodoLists().map((item, index) => {
            return (
              <li
                key={item.id}
                style={{
                  backgroundColor: mouseI === index ? 'pink' : '#fff'
                }}
                onMouseEnter={() => setMouseI(index)}
                onMouseLeave={() => setMouseI(-1)}
              >
                <label>
                  <input
                    type='checkbox'
                    checked={item.done}
                    onChange={event =>
                      handleUpdateTodo(item.id, event.target.checked)
                    }
                  />
                  <span>{item.name}</span>
                </label>
                {mouseI === index && (
                  <button
                    className='btn btn-danger'
                    onClick={() => handleDeleteTodo(item.id)}
                  >
                    删除
                  </button>
                )}
              </li>
            )
          })}
        </ul>
        <div className='todo-footer'>
          <label>
            <input
              type='checkbox'
              checked={
                doneLenth === todolists.length && todolists.length !== 0
                  ? true
                  : false
              }
              onChange={event => handleAllChange(event.target.checked)}
            />{' '}
            全选
          </label>
          <span>
            <span>已完成 {doneLenth}</span> / 全部{todolists.length}
          </span>
          {footerList.map((item, i) => {
            return (
              <span
                className={`btn btn1 ${tabIndex === i ? 'current' : ''}`}
                key={i}
                onClick={() => setTabIndex(i)}
              >
                {item}
              </span>
            )
          })}
          <ClearDone></ClearDone>
        </div>
      </div>
    </div>
  )
}

//使用connect()()创建并暴露一个TodoList的容器组件
export default connect(
  state => {
    return {
      todolists: state.todolist // 导入reducer的名称
    }
  },
  //简写的action, 默认dispatch
  { addTodo, delTodo, updateTodo, allChange, addTodoAsync }
)(TodoList)
